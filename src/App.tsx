import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.scss";
import CatDashboard from "./component/feature/cat-browser/CatDashboard";
import CatDetails from "./component/feature/cat-details/CatDetails";
import BreedProvider from "./component/state/BreedState";

function App() {
  return (
    <BreedProvider>
      <div className="App">
        <BrowserRouter>
          <Routes>
            <Route path="/">
              <Route index element={<CatDashboard />} />
              <Route path=":breedId" element={<CatDetails />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </div>
    </BreedProvider>
  );
}

export default App;
