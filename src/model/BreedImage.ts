export interface BreedImage {
  id: string;
  url: string;
}
