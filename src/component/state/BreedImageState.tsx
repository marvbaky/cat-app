import React, { createContext, useState } from "react";
import { BreedImage } from "../../model/BreedImage";

type IBreedImageContext = [
  BreedImage[],
  React.Dispatch<React.SetStateAction<BreedImage[]>>
];

export const breedImageContext = createContext<IBreedImageContext>([
  [],
  () => [],
]);

const BreedImageProvider = (props: any) => {
  const [breedImages, setBreedImages] = useState<BreedImage[]>([]);

  return (
    <breedImageContext.Provider value={[breedImages, setBreedImages]}>
      {props.children}
    </breedImageContext.Provider>
  );
};

export default BreedImageProvider;
