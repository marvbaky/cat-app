import React, { createContext, useState } from "react";

type IBreedContext = [string, React.Dispatch<React.SetStateAction<string>>];

export const breedContext = createContext<IBreedContext>(["", () => null]);

const BreedProvider = (props: any) => {
  const [breed, setBreed] = useState<string>("");

  return (
    <breedContext.Provider value={[breed, setBreed]}>
      {props.children}
    </breedContext.Provider>
  );
};

export default BreedProvider;
