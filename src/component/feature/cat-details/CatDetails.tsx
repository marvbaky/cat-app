import React, { useContext, useEffect, useState } from "react";
import { Button, Card, Container } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { breedContext } from "../../state/BreedState";
import httpClient from "../../../helper/http/HttpClient";
import { Breed } from "../../../model/Breed";
import { Constants } from "../../../constants/Constants";

const CatDetails = () => {
  const [breed, setBreed] = useContext(breedContext);
  const [breedData, setBreedData] = useState<Breed>();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const navigate = useNavigate();
  const params = useParams();

  useEffect(() => {
    httpClient
      .get(`images/${params.breedId}`)
      .then((res) => {
        const data = res.data;
        setBreedData({
          id: data.id,
          name: data.breeds[0].name,
          origin: data.breeds[0].origin,
          temperament: data.breeds[0].temperament,
          description: data.breeds[0].description,
          imageUrl: data.url,
        } as Breed);

        if (!breed) {
          setBreed(data.breeds[0].id);
        }
        setIsLoading(false);
      })
      .catch(() => alert(Constants.APIErrorMessage));
  }, []);

  const backToDashboard = (path: string) => {
    setBreed('');
    navigate(`${path}?breed=${breed}`);
  };

  return (
    <React.Fragment>
      <div>
        <Container>
          {isLoading ? (
            <h3>Loading...</h3>
          ) : (
            <Card>
              <Card.Header>
                <Button variant="primary" onClick={() => backToDashboard("/")}>
                  Back
                </Button>
              </Card.Header>
              <Card.Img variant="top" src={breedData?.imageUrl} />
              <Card.Body>
                <h4>{breedData?.name}</h4>
                <h5>Origin: {breedData?.origin}</h5>
                <h6>{breedData?.temperament}</h6>
                <p>{breedData?.description}</p>
              </Card.Body>
            </Card>
          )}
        </Container>
      </div>
    </React.Fragment>
  );
};

export default CatDetails;
