import React, { useContext } from "react";
import { Button } from "react-bootstrap";
import { BreedImage } from "../../../model/BreedImage";
import { breedImageContext } from "../../state/BreedImageState";
import { breedContext } from "../../state/BreedState";
import httpClient from "../../../helper/http/HttpClient";
import { Constants } from "../../../constants/Constants";

type Props = {
  isLoading: boolean;
  maxBreedImageRetrieved: boolean;
  setIsLoading: (value: boolean) => void;
  setMaxBreedImageRetrieved: (value: boolean) => void;
};

const CatMoreButton = (props: Props) => {
  const [breed] = useContext(breedContext);
  const [breedImages, setBreedImages] = useContext(breedImageContext);
  const isDisabled = !breed || props.isLoading;

  const LoadMoreCats = () => {
    if (breed) {
      props.setIsLoading(true);
      httpClient
        .get(`images/search?breed_id=${breed}&limit=10`)
        .then((res) => {
          const data = res.data;
          const dataParsed: BreedImage[] = data.map((d: any) => {
            return {
              id: d.id,
              url: d.url,
            } as BreedImage;
          });

          const dataToBeAdded: BreedImage[] = [];
          dataParsed.forEach((b: BreedImage) => {
            if (!breedImages.some((bis) => bis.id === b.id)) {
              dataToBeAdded.push(b);
            }
          });

          if (dataToBeAdded.length > 0) {
            setBreedImages([...breedImages, ...dataToBeAdded]);
          } else {
            props.setMaxBreedImageRetrieved(true);
          }

          props.setIsLoading(false);
        })
        .catch(() => alert(Constants.APIErrorMessage));
    }
  };

  return (
    <React.Fragment>
      {props.maxBreedImageRetrieved ? (
        ""
      ) : (
        <Button
          className="mt-3"
          variant="success"
          disabled={isDisabled}
          onClick={LoadMoreCats}
        >
          {props.isLoading ? "Loading cats..." : "Load more"}
        </Button>
      )}
    </React.Fragment>
  );
};

export default CatMoreButton;
