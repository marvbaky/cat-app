import React, { useContext, useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import CatList from "./CatList";
import CatMoreButton from "./CatMoreButton";
import CatSelect from "./CatSelect";
import BreedImageProvider from "../../../component/state/BreedImageState";
import { useSearchParams } from "react-router-dom";
import { breedContext } from "../../state/BreedState";

const CatDashboard = () => {
  const [breed, setBreed] = useContext(breedContext);
  const [isLoading, setIsLoading] = useState(false);
  const [maxBreedImageRetrieved, setMaxBreedImageRetrieved] = useState(false);
  const [hasBreedsLoaded, setHasBreedsLoaded] = useState(false);

  const [searchParams] = useSearchParams();

  useEffect(() => {
    if (hasBreedsLoaded) {
      const breedParam = searchParams.get("breed") ?? "";
      if (breedParam !== breed) {
        setBreed(breedParam);
      }
    }
  }, [searchParams, hasBreedsLoaded]);

  const updateLoadingState = (value: boolean) => {
    setIsLoading(value);
  };

  const updateMaxBreedImageRetrievedState = (value: boolean) => {
    setMaxBreedImageRetrieved(value);
  };

  const updateHasBreedsLoadedState = (value: boolean) => {
    setHasBreedsLoaded(value);
  };

  return (
    <div>
      <Container>
        <h1>Cat Browser</h1>
        <Row>
          <Col xs={3}>
            <CatSelect setHasBreedsLoaded={updateHasBreedsLoadedState} />
          </Col>
        </Row>
        <BreedImageProvider>
          <Row>
            <Col>
              <CatList
                hasBreedsLoaded={hasBreedsLoaded}
                setIsLoading={updateLoadingState}
                setMaxBreedImageRetrieved={updateMaxBreedImageRetrievedState}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <CatMoreButton
                isLoading={isLoading}
                maxBreedImageRetrieved={maxBreedImageRetrieved}
                setIsLoading={updateLoadingState}
                setMaxBreedImageRetrieved={updateMaxBreedImageRetrievedState}
              />
            </Col>
          </Row>
        </BreedImageProvider>
      </Container>
    </div>
  );
};

export default CatDashboard;
