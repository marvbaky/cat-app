import React, { useContext, useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import { Constants } from "../../../constants/Constants";
import httpClient from "../../../helper/http/HttpClient";
import { breedContext } from "../../state/BreedState";

type Props = {
  setHasBreedsLoaded: (value: boolean) => void;
};

const CatSelect = (props: Props) => {
  const [breed, setBreed] = useContext(breedContext);
  const [breeds, setBreeds] = useState([]);

  useEffect(() => {
    props.setHasBreedsLoaded(false);
    httpClient
      .get(`breeds/`)
      .then((res) => {
        const data = res.data;
        setBreeds(
          data.map((breed: any) => {
            return {
              id: breed.id,
              name: breed.name,
            };
          })
        );
      })
      .catch(() => alert(Constants.APIErrorMessage));
  }, []);

  useEffect(() => {
    return () => props.setHasBreedsLoaded(true);
  }, [breeds]);

  const renderBreedSelect = () => {
    return breeds.map((breed: any) => {
      return (
        <option key={breed.id} value={breed.id}>
          {breed.name}
        </option>
      );
    });
  };

  const updateBreedContext = (value: string) => {
    setBreed(value);
  };

  return (
    <Form.Group className="mb-3">
      <Form.Label>Breed</Form.Label>
      <Form.Select
        aria-label="Breed"
        onChange={(e) => {
          updateBreedContext(e.target.value);
        }}
        value={breed}
      >
        <option value="">Select breed</option>
        {renderBreedSelect()}
      </Form.Select>
    </Form.Group>
  );
};

export default CatSelect;
