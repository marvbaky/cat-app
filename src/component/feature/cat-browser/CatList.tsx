import React, { useContext, useEffect } from "react";
import { Col, Row } from "react-bootstrap";
import { Constants } from "../../../constants/Constants";
import httpClient from "../../../helper/http/HttpClient";
import { BreedImage } from "../../../model/BreedImage";
import { breedImageContext } from "../../state/BreedImageState";
import { breedContext } from "../../state/BreedState";
import CatCard from "./CatCard";

type Props = {
  hasBreedsLoaded: boolean;
  setIsLoading: (value: boolean) => void;
  setMaxBreedImageRetrieved: (value: boolean) => void;
};

const CatList = (props: Props) => {
  const [breed] = useContext(breedContext);
  const [breedImages, setBreedImages] = useContext(breedImageContext);
  const { hasBreedsLoaded } = props;

  useEffect(() => {
    setBreedImages([]);
    props.setMaxBreedImageRetrieved(false);
    if (breed && props.hasBreedsLoaded) {
      props.setIsLoading(true);
      httpClient
        .get(`images/search?breed_id=${breed}&limit=10`)
        .then((res) => {
          const data = res.data;
          setBreedImages(
            data.map((d: any) => {
              return {
                id: d.id,
                url: d.url,
              } as BreedImage;
            })
          );
          props.setIsLoading(false);
        })
        .catch(() => alert(Constants.APIErrorMessage));
    }
  }, [breed, hasBreedsLoaded]);

  const renderCatCard = () => {
    if (breedImages.length > 0) {
      return breedImages.map((breedImage) => {
        return (
          <Col key={breedImage.id} className="col-12" md={3} sm={6}>
            <CatCard {...breedImage}></CatCard>
          </Col>
        );
      });
    } else {
      return (
        <Col xs={3}>
          <p className="mb-3">No cats available</p>
        </Col>
      );
    }
  };

  return <Row>{renderCatCard()}</Row>;
};

export default CatList;
