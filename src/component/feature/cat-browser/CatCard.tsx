import React from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { BreedImage } from "../../../model/BreedImage";

const CatCard = (props: BreedImage) => {
  const navigate = useNavigate();

  const goToDetails = (path: string) => {
    navigate(`${path}${props.id}`);
  };

  return (
    <Card>
      <Card.Img variant="top" src={props.url} />
      <Card.Body>
        <div className="d-grid gap-2">
          <Button variant="primary" onClick={() => goToDetails("/")}>
            View Details
          </Button>
        </div>
      </Card.Body>
    </Card>
  );
};

export default CatCard;
