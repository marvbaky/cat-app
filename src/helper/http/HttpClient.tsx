import axios from "axios";

//You may uncomment headers if you ran to any issues with authorization.
//I just commented it because of 10000 limits on the API requests
//But seems to be working with or without the headers
export default axios.create({
  baseURL: `https://api.thecatapi.com/v1/`,
  // headers: {
  //     'x-api-key': '4efd4fb9-4dda-4773-87ac-65089b0c9055'
  //   }
});
